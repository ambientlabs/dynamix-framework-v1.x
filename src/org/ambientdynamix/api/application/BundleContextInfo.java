/*
 * Copyright (C) The Ambient Dynamix Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ambientdynamix.api.application;

import java.util.Set;
import java.util.TreeSet;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * IContextInfo implementation that exposes a single Android Bundle.
 * 
 * @author Darren Carlson
 * 
 */
public class BundleContextInfo implements IBundleContextInfo {
	public static String DYNAMIX_CONTEXT_TYPE_KEY = "DYNAMIX_CONTEXT_TYPE_KEY";
	private final String TAG = this.getClass().getSimpleName();
	/**
	 * Required CREATOR field that generates instances of this Parcelable class from a Parcel.
	 * 
	 * @see http://developer.android.com/reference/android/os/Parcelable.Creator.html
	 */
	public static Parcelable.Creator<BundleContextInfo> CREATOR = new Parcelable.Creator<BundleContextInfo>() {
		/**
		 * Create a new instance of the Parcelable class, instantiating it from the given Parcel whose data had
		 * previously been written by Parcelable.writeToParcel().
		 */
		public BundleContextInfo createFromParcel(Parcel in) {
			return new BundleContextInfo(in);
		}

		/**
		 * Create a new array of the Parcelable class.
		 */
		public BundleContextInfo[] newArray(int size) {
			return new BundleContextInfo[size];
		}
	};
	// Private data
	private Bundle data;

	/**
	 * Creates a BundleContextInfo
	 */
	public BundleContextInfo(Bundle data, String contextType) {
		this.data = data;
		if (data != null) {
			data.putString(DYNAMIX_CONTEXT_TYPE_KEY, contextType);
		} else
			Log.w(TAG, "Bundle was null in BundleContextInfo for context type " + contextType);
	}

	/**
	 * Creates a BundleContextInfo
	 */
	private BundleContextInfo(Parcel in) {
		this.data = in.readBundle();
	}

	/**
	 * Returns the Bundle associated with this IContextInfo
	 */
	public Bundle getData() {
		return this.data;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int describeContents() {
		return 0;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void writeToParcel(Parcel out, int flags) {
		out.writeBundle(this.data);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getContextType() {
		if (data != null)
			return data.getString(DYNAMIX_CONTEXT_TYPE_KEY);
		else
			return "NO_CONTEXT_TYPE_GIVEN";
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getImplementingClassname() {
		return this.getClass().getName();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@JsonIgnore
	public Set<String> getStringRepresentationFormats() {
		return new TreeSet<String>();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@JsonIgnore
	public String getStringRepresentation(String format) {
		return "";
	}
}
