/*
 * Copyright (C) The Ambient Dynamix Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ambientdynamix.api.application;

import java.util.List;

import android.os.RemoteException;

/**
 * A DynamixListener helper-class that implements all IDynamixListener methods without introducing any behavior (i.e.,
 * the methods do nothing). This allows clients to override only the methods they're interested in implementing.
 * 
 * @author Darren Carlson
 * 
 */
public class DynamixListenerBase extends IDynamixListener.Stub {
	@Override
	public void onDynamixListenerAdded(String listenerId) throws RemoteException {
		// TODO Auto-generated method stub
	}

	@Override
	public void onDynamixListenerRemoved() throws RemoteException {
		// TODO Auto-generated method stub
	}

	@Override
	public void onAwaitingSecurityAuthorization() throws RemoteException {
		// TODO Auto-generated method stub
	}

	@Override
	public void onSecurityAuthorizationGranted() throws RemoteException {
		// TODO Auto-generated method stub
	}

	@Override
	public void onSecurityAuthorizationRevoked() throws RemoteException {
		// TODO Auto-generated method stub
	}

	@Override
	public void onSessionOpened(String sessionId) throws RemoteException {
		// TODO Auto-generated method stub
	}

	@Override
	public void onSessionClosed() throws RemoteException {
		// TODO Auto-generated method stub
	}

	@Override
	public void onContextEvent(ContextEvent event) throws RemoteException {
		// TODO Auto-generated method stub
	}

	@Override
	public void onContextSupportAdded(ContextSupportInfo supportInfo) throws RemoteException {
		// TODO Auto-generated method stub
	}

	@Override
	public void onContextSupportRemoved(ContextSupportInfo supportInfo) throws RemoteException {
		// TODO Auto-generated method stub
	}

	@Override
	public void onContextTypeNotSupported(String contextType) throws RemoteException {
		// TODO Auto-generated method stub
	}

	@Override
	public void onInstallingContextSupport(ContextPluginInformation plugin, String contextType) throws RemoteException {
		// TODO Auto-generated method stub
	}

	@Override
	public void onInstallingContextPlugin(ContextPluginInformation plugin) throws RemoteException {
		// TODO Auto-generated method stub
	}

	@Override
	public void onContextPluginInstallProgress(ContextPluginInformation plugin, int percentComplete)
			throws RemoteException {
		// TODO Auto-generated method stub
	}

	@Override
	public void onContextPluginInstalled(ContextPluginInformation plugin) throws RemoteException {
		// TODO Auto-generated method stub
	}

	@Override
	public void onContextPluginUninstalled(ContextPluginInformation plugin) throws RemoteException {
		// TODO Auto-generated method stub
	}

	@Override
	public void onContextPluginInstallFailed(ContextPluginInformation plug, String message) throws RemoteException {
		// TODO Auto-generated method stub
	}

	@Override
	public void onContextRequestFailed(String requestId, String message, int errorCode) throws RemoteException {
		// TODO Auto-generated method stub
	}

	@Override
	public void onContextPluginDiscoveryStarted() throws RemoteException {
		// TODO Auto-generated method stub
	}

	@Override
	public void onContextPluginDiscoveryFinished(List<ContextPluginInformation> discoveredPlugins)
			throws RemoteException {
		// TODO Auto-generated method stub
	}

	@Override
	public void onDynamixFrameworkActive() throws RemoteException {
		// TODO Auto-generated method stub
	}

	@Override
	public void onDynamixFrameworkInactive() throws RemoteException {
		// TODO Auto-generated method stub
	}

	@Override
	public void onContextPluginError(ContextPluginInformation plug, String message) throws RemoteException {
		// TODO Auto-generated method stub
	}

	@Override
	public void onContextPluginEnabled(ContextPluginInformation plug) throws RemoteException {
		// TODO Auto-generated method stub
	}

	@Override
	public void onContextPluginDisabled(ContextPluginInformation plug) throws RemoteException {
		// TODO Auto-generated method stub
	}

	@Override
	public void onContextSupportResult(ContextSupportResult result) throws RemoteException {
		// TODO Auto-generated method stub
	}
}
