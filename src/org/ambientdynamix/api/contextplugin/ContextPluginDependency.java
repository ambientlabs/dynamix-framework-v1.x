/*
 * Copyright (C) The Ambient Dynamix Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ambientdynamix.api.contextplugin;

import java.io.Serializable;

import org.ambientdynamix.api.application.VersionInfo;

/**
 * Represents basic information about a plug-in dependency.
 * 
 * @author Darren Carlson
 * 
 */
public class ContextPluginDependency implements Serializable {
	private static final long serialVersionUID = 5284958004227230652L;
	private String pluginId;
	private VersionInfo pluginVersion = new VersionInfo(0, 0, 0);
	private String pluginName;
	private String installUrl;

	public ContextPluginDependency() {
	}

	public ContextPluginDependency(String pluginId) {
		this.pluginId = pluginId;
	}

	public ContextPluginDependency(String pluginId, VersionInfo pluginVersion) {
		this.pluginId = pluginId;
		this.pluginVersion = pluginVersion;
	}

	public ContextPluginDependency(String pluginId, VersionInfo pluginVersion, String pluginName, String installUrl) {
		this.pluginId = pluginId;
		this.pluginVersion = pluginVersion;
		this.pluginName = pluginName;
		this.installUrl = installUrl;
	}

	public String getPluginId() {
		return pluginId;
	}

	public void setPluginId(String id) {
		this.pluginId = id;
	}

	public VersionInfo getPluginVersion() {
		return pluginVersion;
	}

	public void setVersion(VersionInfo version) {
		this.pluginVersion = version;
	}

	public String getPluginName() {
		return pluginName;
	}

	public void setPluginName(String pluginName) {
		this.pluginName = pluginName;
	}

	public String getInstallUrl() {
		return installUrl;
	}

	public void setInstallUrl(String installUrl) {
		this.installUrl = installUrl;
	}
	
	@Override
	public String toString() {
		return "Dependency for " + pluginId;
	}
	
	@Override
	public boolean equals(Object candidate) {
		// Check for null
		if (candidate == null)
			return false;
		// Determine if they are the same object reference
		if (this == candidate)
			return true;
		// Make sure they are the same class
		if (candidate == null || candidate.getClass() != getClass())
			return false;
		// Make sure the id's and version numbers are the same
		ContextPluginDependency other = (ContextPluginDependency) candidate;
		if (other.getPluginId().equalsIgnoreCase(this.getPluginId()))
			if (other.getPluginVersion().equals(this.getPluginVersion()))
				return true;
		return false;
	}


	@Override
	public int hashCode() {
		int result = 17;
		result = 31 * result + getPluginId().hashCode() + getPluginVersion().hashCode();
		return result;
	}
}