/*
 * Copyright (C) The Ambient Dynamix Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ambientdynamix.api.contextplugin.security;

import java.util.Set;

import android.Manifest;
import android.content.Intent;
import android.util.Log;

/**
 * The permissions handler that is injected into a SecuredContext by Dynamix. Only Dynamix is provided access to the
 * permissions handler, with the intention of preventing plug-ins from changing their own permissions.
 * 
 * @author Darren Carlson
 * 
 */
public final class PermissionsHandler {
	// Private data
	private final String TAG = this.getClass().getSimpleName();
	private volatile Set<Permission> permissions;
	private volatile boolean permissionCheckingEnabled = true;

	/**
	 * Creates a PermissionsHandler
	 */
	public PermissionsHandler(boolean permissionCheckingEnabled) {
		this.permissionCheckingEnabled = permissionCheckingEnabled;
	}

	/**
	 * Creates a PermissionsHandler
	 */
	public PermissionsHandler(Set<Permission> permissions, boolean permissionCheckingEnabled) {
		this.permissions = permissions;
		this.permissionCheckingEnabled = permissionCheckingEnabled;
	}

	/**
	 * Clears all permissions.
	 */
	public void clearAllPermissions() {
		this.permissions = null;
	}

	/**
	 * Replaces the set of permissions with the incoming set. Note that old permissions are not maintained.
	 * 
	 * @param permissions
	 *            The new set of permissions to apply.
	 */
	public void updatePermissions(Set<Permission> permissions) {
		this.permissions = permissions;
	}

	/**
	 * Returns true if permission checking is enabled; false otherwise.
	 */
	public boolean isPermissionCheckingEnabled() {
		return this.permissionCheckingEnabled;
	}

	/**
	 * Set true if permission checking is enabled; false otherwise.
	 */
	public void setPermissionCheckingEnabled(boolean enabled) {
		this.permissionCheckingEnabled = enabled;
	}

	/**
	 * Returns the current list of permissions.
	 */
	public Set<Permission> getPermissions() {
		return this.permissions;
	}

	/**
	 * Returns true if the Activity referenced by the Intent is allowed to be started; false otherwise.
	 */
	public boolean isActivityStartAllowed(Intent intent) {
		/*
		 * This method will require a flexible solution that can be potentially updated from a rules repository on the
		 * Dynamix repo. For the moment, we are only doing simple checking for CALL_PHONE and SEND_SMS permissions using
		 * the intent's data uri scheme.
		 */
		if (intent != null) {
			String uri = intent.getDataString();
			if (uri != null) {
				uri = uri.trim().toLowerCase();
				if (uri.startsWith("tel")) {
					return checkPermission(Manifest.permission.CALL_PHONE);
				} else if (uri.startsWith("sms")) {
					return checkPermission(Manifest.permission.SEND_SMS);
				}
			}
		}
		return false;
	}

	/**
	 * Returns true if the incoming permissions string is granted; false otherwise.
	 */
	public boolean checkPermission(String permString) {
		Log.d(TAG, "Checking permissions for: " + permString);
		if (isPermissionCheckingEnabled() && getPermissions() != null) {
			for (Permission p : getPermissions()) {
				if (p.getPermissionString().equalsIgnoreCase(permString) && p.isPermissionGranted()) {
					Log.d(TAG, "Permission granted: " + permString);
					return true;
				}
			}
		} else {
			Log.d(TAG, "Permission checking disabled... granting: " + permString);
			return true;
		}
		Log.w(TAG, "Permission NOT granted: " + permString);
		return false;
	}
}
