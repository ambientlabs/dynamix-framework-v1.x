/*
 * Copyright (C) The Ambient Dynamix Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ambientdynamix.api.contextplugin;

import java.util.UUID;

import android.os.Bundle;

/**
 * Simple interface for message result handling (e.g., during intra-plug-in messaging).
 * 
 * @author Darren Carlson
 * 
 */
public interface IMessageResultHandler {
	/**
	 * Raised when a message result is available for the specified request id.
	 * 
	 * @param requestId
	 *            The id of the originating message request.
	 * @param result
	 *            The result as an Android Bundle.
	 */
	public void onResult(UUID requestId, Bundle result);

	/**
	 * Raised if a message request fails.
	 * 
	 * @param requestId
	 *            The id of the originating message request.
	 */
	public void onFailure(UUID requestId, String message);
}
