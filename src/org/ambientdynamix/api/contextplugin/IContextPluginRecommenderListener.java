/*
 * Copyright (C) The Ambient Dynamix Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ambientdynamix.api.contextplugin;

import java.util.List;

import org.ambientdynamix.api.application.ContextPluginInformation;

/**
 * Interface for context plug-in recommendations.
 * 
 * @author Darren Carlson
 * 
 */
public interface IContextPluginRecommenderListener {
	/**
	 * Provides a specific PluginRecommendation.
	 * 
	 * @param recommendation
	 *            The PluginRecommendation.
	 * @param deprecateExistingPlugins
	 *            True if other plug-ins handling the context support registration should be deprecated; false if not
	 *            (i.e., to maintain them in addition to the recommendation).
	 */
	public void onPluginRecommended(PluginRecommendation recommendation, boolean deprecateExistingPlugins);

	/**
	 * Provides a list of PluginRecommendations.
	 * 
	 * @param recommendations
	 *            The PluginRecommendations.
	 * @param deprecateExistingPlugins
	 *            True if other plug-ins handling the context support registration should be deprecated; false if not
	 *            (i.e., to maintain them in addition to the recommendations).
	 */
	public void onPluginsRecommended(List<PluginRecommendation> recommendations, boolean deprecateExistingPlugins);

	/**
	 * Indicates that the specified plug-in should be deprecated (i.e., the plug-in is no longer recommended for
	 * handling context support registrations).
	 * 
	 * @param plug
	 *            The plug-in to be deprecated.
	 */
	public void onPluginDeprecated(ContextPluginInformation plug);

	/**
	 * Indicates that the list of plug-ins should be deprecated (i.e., the plug-in is no longer recommended for handling
	 * context support registrations).
	 * 
	 * @param plug
	 *            The plug-ins to be deprecated.
	 */
	public void onPluginsDeprecated(List<ContextPluginInformation> plugs);
}
