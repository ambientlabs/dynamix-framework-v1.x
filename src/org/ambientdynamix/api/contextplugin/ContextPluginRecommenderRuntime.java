/*
 * Copyright (C) The Ambient Dynamix Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ambientdynamix.api.contextplugin;

import java.util.List;
import java.util.Vector;

import org.ambientdynamix.api.application.ContextPluginInformation;
import org.ambientdynamix.util.Utils;

import android.util.Log;

/**
 * Experimental base class for plug-in recommenders, which are capable of orchestrating the installation of plug-ins
 * that are considered the most useful in fulfilling the needs of a given context support request.
 * 
 * @author Darren Carlson
 * 
 */
public abstract class ContextPluginRecommenderRuntime extends AutoReactiveContextPluginRuntime {
	private final String TAG = this.getClass().getSimpleName();
	private final List<IContextPluginRecommenderListener> listeners = new Vector<IContextPluginRecommenderListener>();

	public abstract List<PluginRecommendation> getRecommendedPlugins();

	public final void addContextPluginRecommenderListener(IContextPluginRecommenderListener listener) {
		if (!listeners.contains(listener))
			listeners.add(listener);
	}

	public final void removeContextPluginRecommenderListener(IContextPluginRecommenderListener listener) {
		listeners.remove(listener);
	}

	protected final void clearListeners() {
		this.listeners.clear();
	}

	protected final void notifyPluginRecommended(final PluginRecommendation plug, final boolean deprecateExistingPlugins) {
		synchronized (listeners) {
			for (final IContextPluginRecommenderListener listener : listeners)
				doNotify(listener, new Runnable() {
					@Override
					public void run() {
						Log.d(TAG, "Notify plug-in recommended: " + plug + " with deprecateExistingPlugins "
								+ deprecateExistingPlugins);
						listener.onPluginRecommended(plug, deprecateExistingPlugins);
					}
				});
		}
	}

	protected final void notifyPluginsRecommended(final List<PluginRecommendation> plugs,
			final boolean deprecateExistingPlugins) {
		synchronized (listeners) {
			for (final IContextPluginRecommenderListener listener : listeners)
				doNotify(listener, new Runnable() {
					@Override
					public void run() {
						listener.onPluginsRecommended(plugs, deprecateExistingPlugins);
					}
				});
		}
	}

	protected final void notifyPluginDeprecated(final ContextPluginInformation plug) {
		synchronized (listeners) {
			for (final IContextPluginRecommenderListener listener : listeners)
				doNotify(listener, new Runnable() {
					@Override
					public void run() {
						Log.d(TAG, "Notify plug-in deprecated: " + plug);
						listener.onPluginDeprecated(plug);
					}
				});
		}
	}

	protected final void notifyPluginDeprecated(final List<ContextPluginInformation> plugs) {
		synchronized (listeners) {
			for (final IContextPluginRecommenderListener listener : listeners)
				doNotify(listener, new Runnable() {
					@Override
					public void run() {
						listener.onPluginsDeprecated(plugs);
					}
				});
		}
	}

	private final void doNotify(final IContextPluginRecommenderListener listener, final Runnable event) {
		if (listener != null) {
			if (event != null) {
				if (this.listeners.contains(listener)) {
					Utils.dispatch(true, event);
				} else {
					Log.w(TAG, "Listener was unknown: " + listener);
				}
			} else
				Log.w(TAG, "event was null... aborting notifyListener");
		} else
			Log.w(TAG, "listener was null... aborting notifyListener");
	}
}
