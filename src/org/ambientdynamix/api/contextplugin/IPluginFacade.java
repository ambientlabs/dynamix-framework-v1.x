/*
 * Copyright (C) The Ambient Dynamix Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ambientdynamix.api.contextplugin;

import java.util.List;
import java.util.UUID;

import org.ambientdynamix.api.application.ContextPluginInformation;
import org.ambientdynamix.api.application.IDynamixFacade;
import org.ambientdynamix.api.application.IdResult;
import org.ambientdynamix.util.ContextRequest;

import android.content.Context;
import android.os.Bundle;

/**
 * The IPluginFacade provides a mechanism whereby a ContextPluginRuntime can securely interact with Dynamix and Android.
 * ContextPluginRuntimes never interact with Android directly; rather, Android interaction is mediated through
 * implementations of the IPluginFacade, which check if the calling client is authorized to receive a given service,
 * register/unregister a particular BroadcastReceiver, etc. For example, obtaining a secured version of an Android
 * Context, which can be used to access accessing a particular Android service (e.g. Context.LOCATION_SERVICE).
 * 
 * @author Darren Carlson
 */
public interface IPluginFacade {
	/**
	 * Returns the ContextPluginInformation associated with the plug-in;
	 * 
	 * @param sessionID
	 *            The unique session id of the calling ContextPluginRuntime.
	 */
	public ContextPluginInformation getPluginInfo(UUID sessionID);

	/**
	 * Closes any configuration view associated with the Context Plugin.
	 * 
	 * @param sessionID
	 *            The unique session id of the calling ContextPluginRuntime.
	 */
	public void closeConfigurationView(UUID sessionID);

	/**
	 * Closes any context acquisition view associated with the Context Plugin.
	 * 
	 * @param sessionID
	 *            The unique session id of the calling ContextPluginRuntime.
	 */
	public void closeContextAcquisitionView(UUID sessionID);

	/**
	 * Returns the ContextPluginSettings persisted for the given ContextPluginRuntime in the Dynamix Framework.
	 * 
	 * @param sessionID
	 *            The unique session id of the calling ContextPluginRuntime Returns a ContextPlugin-specific
	 *            ContextPluginSettings object.
	 */
	public ContextPluginSettings getContextPluginSettings(UUID sessionID);

	/**
	 * Returns a secured version of the Android Context that is customized for the caller. The SecuredContext is
	 * configured by a set of Permissions, which allow only specific actions that have been granted by the user.
	 * 
	 * @param sessionID
	 *            The unique session id of the calling ContextPluginRuntime Returns a configured SecuredContext, or null
	 *            if the sessionID is incorrect
	 */
	public Context getSecuredContext(UUID sessionID);

	/**
	 * Returns the current PluginState for the ContextPluginRuntime
	 * 
	 * @param sessionID
	 *            The unique session id of the calling ContextPluginRuntime
	 * @return The state of the calling ContextPluginRuntime
	 */
	public PluginState getState(UUID sessionID);

	/**
	 * Sets the ContextPluginRuntime's associated ContextPlugin to the configured or unconfigured state.
	 * 
	 * @param sessionID
	 *            The unique session id of the calling ContextPluginRuntime.
	 * @param configured
	 *            True if the plugin is properly configured; false otherwise Returns true if the configuration status
	 *            was set; false otherwise.
	 */
	public boolean setPluginConfiguredStatus(UUID sessionID, boolean configured);

	/**
	 * Stores the ContextPluginSettings in the Dynamix Framework on behalf on the calling ContextPluginRuntime.
	 * 
	 * @param sessionID
	 *            The unique session id of the calling ContextPluginRuntime
	 * @param settings
	 *            The ContextPluginSettings to store Returns true if the settings were successfully stored; false
	 *            otherwise.
	 */
	public boolean storeContextPluginSettings(UUID sessionID, ContextPluginSettings settings);

	/**
	 * Adds a listener that will receive NFC events from Android
	 * 
	 * @param sessionID
	 *            The unique session id of the calling ContextPluginRuntime.
	 * @param listener
	 *            The listener that should receive the event (as an Intent)
	 * @return True if the listener was added; false otherwise.
	 */
	public boolean addNfcListener(UUID sessionID, NfcListener listener);

	/**
	 * Removes a previously added NfcListener
	 * 
	 * @param sessionID
	 *            The unique session id of the calling ContextPluginRuntime.
	 * @param listener
	 *            The NfcListener that should be removed.
	 * @return True if the listener was removed; false otherwise.
	 */
	public boolean removeNfcListener(UUID sessionID, NfcListener listener);

	/**
	 * Sends a PluginAlert to Dynamix, which will show it to the user if the plug-in has permission.
	 * 
	 * @param sessionID
	 *            The unique session id of the calling ContextPluginRuntime.
	 * @param alert
	 *            The PluginAlert
	 * @return True if the alert was sent; false otherwise.
	 */
	public boolean sendPluginAlert(UUID sessionID, PluginAlert alert);

	/**
	 * Establishes a persistent request channel between the plug-in and the requesting app, meaning that Dynamix will
	 * keep the request alive as long as 'pingRequest' is called before the request timeout occurs. This enables
	 * plug-ins to provide long running processing and/or return multiple events to the same requestId. To close a
	 * persistent request channel call 'closePersistentRequestChannel' or stop pinging the requestId.
	 * 
	 * @param requestId
	 *            The request if of the persistent channel to open.
	 * @return True if the persistent request channel was opened; false otherwise.
	 */
	public boolean openPersistentRequestChannel(UUID requestId);

	/**
	 * Imediately closes a previously opened request channel, meaning that no additional events can be sent from the
	 * plug-in to the app.
	 * 
	 * @param requestId
	 *            The request id of the persistent channel to close.
	 * @return True if the persistent request channel was closed; false otherwise.
	 */
	public boolean closePersistentRequestChannel(UUID requestId);

	/**
	 * Indicates that Dynamix should ping the request, which keeps previously established persistent request channels
	 * alive for the default timeout interval (e.g., 30 seconds). Note that plug-ins should call this method *before*
	 * the default timeout passes if they require additional processing time to ensure they are not removed. This method
	 * only has an effect for if 'openPersistentRequestChannel' has been called for the given requestId.
	 * 
	 * @param requestId
	 *            The requestId of the persistent request channel to ping.
	 * @return True if the persistent request channel was found and pinged; false otherwise.
	 */
	public boolean pingRequest(UUID requestId);

	/**
	 * Sends a message Bundle to the specified plug-in.
	 * 
	 * @param sessionID
	 *            The unique session id of the calling ContextPluginRuntime.
	 * @param pluginId
	 *            The id of the plug-in receiver.
	 * @param messageBundle
	 *            The message Bundle to send.
	 * @param resultHandler
	 *            A handler for message results (can be null).
	 * @return An IdResult indicating if the message was sent or not. If it was sent, the IdResult's id is the requestId
	 *         that can be used to differentiate associated results in an IMessageResultHandler.
	 */
	public IdResult sendMessage(UUID sessionID, String pluginId, Bundle messageBundle, IMessageResultHandler resultHandler);

	/**
	 * Returns the number of milliseconds that a context request is considered valid before a timeout. Before a timeout,
	 * a plug-in may respond to a context request and the event will be sent; however, after the request timeout occurs,
	 * any subsequent requests will be blocked from reaching the calling app. Plug-ins requiring additional processing
	 * time should 'ping' Dynamis using the 'pingRequest', which extends the time alloted to the request by the timeout
	 * amount. Plug-ins may continue to ping Dynamix for as long as they require a request channel to remain open.
	 */
	public int getRequestTimeoutMills();

	/**
	 * Removes the context request id from Dynamix, which helps Dynamix lower its memory usage. This functionality
	 * should be used if a plug-in receives a context request for an action that will not generate a context event. In
	 * this case, the plug-in should cancel the request id using this method.
	 * 
	 * @param sessionID
	 *            The unique session id of the calling ContextPluginRuntime.
	 * @param requestId
	 *            The context requestId to be cancelled.
	 * @return True if the request was cancelled; false otherwise.
	 */
	public boolean cancelContextRequestId(UUID sessionID, UUID requestId);

	/**
	 * Returns a list of currently ongoing context requests that are registered to the sessionId and contextType.
	 */
	public List<ContextRequest> getContextRequests(UUID sessionID, String contextType);

	/**
	 * Returns the IDynamixFacade, making it possible for plug-ins and other types of "internal" Dynamix classes to
	 * register for context support (e.g., a plug-in can request services from another plug-in).
	 * 
	 * @param sessionID
	 *            The unique session id of the calling ContextPluginRuntime.
	 * @return A IDynamixFacade or null if the caller is not allowed to access Dynamix the IDynamixFacade.
	 */
	public IDynamixFacade getDynamixFacade(UUID sessionID);
}