/*
 * Copyright (C) The Ambient Dynamix Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ambientdynamix.core;

import java.util.List;

import org.ambientdynamix.api.application.ContextEvent;
import org.ambientdynamix.api.application.ContextPluginInformation;
import org.ambientdynamix.api.application.ContextSupportInfo;
import org.ambientdynamix.api.application.ContextSupportResult;
import org.ambientdynamix.api.application.IDynamixListener;
import org.ambientdynamix.api.contextplugin.SimpleBinder;

import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;

/**
 * Enables internal Dynamix classes, such as plug-ins, to listen for other plug-ins' events.
 * 
 * @author Darren Carlson
 * 
 */
public class InternalDynamixListener implements IDynamixListener {
	private static String TAG = InternalDynamixListener.class.getSimpleName();
	private IBinder binder;
	private int identityHash;
	private IDynamixListener listener;
	private String appId;
	private String className;

	/**
	 * Creates an InternalDynamixListener for the specified appId and listener.
	 */
	public InternalDynamixListener(String appId, IDynamixListener listener) {
		this.appId = appId;
		this.className = listener.getClass().getName();
		this.identityHash = System.identityHashCode(listener);
		this.listener = listener;
		this.binder = new SimpleBinder(getInternalListenerId());
		Log.d(TAG, "Created InternalDynamixListener for " + getInternalListenerId());
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean equals(Object candidate) {
		// First determine if they are the same object reference
		if (this == candidate)
			return true;
		// Make sure they are the same class
		if (candidate == null || candidate.getClass() != getClass())
			return false;
		// Ok, they are the same class... check if their ids are the same
		InternalDynamixListener other = (InternalDynamixListener) candidate;
		return this.getInternalListenerId().equalsIgnoreCase(other.getInternalListenerId());
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int hashCode() {
		int result = 17;
		result = 31 * result + this.getInternalListenerId().hashCode();
		return result;
	}

	/**
	 * Returns the internal id of the listener.
	 */
	final public String getInternalListenerId() {
		/*
		 * Note that the identity of the calling class may not be unique, but will be unique among 'live' objects. See
		 * http://stackoverflow.com/questions/909843/java-how-to-get-the-unique-id-of-an-object-which-overrides-hashcode
		 */
		return appId + "-" + className + "-" + identityHash;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public IBinder asBinder() {
		return binder;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void onDynamixListenerAdded(String listenerId) throws RemoteException {
		listener.onDynamixListenerAdded(listenerId);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void onDynamixListenerRemoved() throws RemoteException {
		listener.onDynamixListenerRemoved();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void onAwaitingSecurityAuthorization() throws RemoteException {
		listener.onAwaitingSecurityAuthorization();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void onSecurityAuthorizationGranted() throws RemoteException {
		listener.onSecurityAuthorizationGranted();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void onSecurityAuthorizationRevoked() throws RemoteException {
		listener.onSecurityAuthorizationRevoked();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void onSessionOpened(String sessionId) throws RemoteException {
		listener.onSessionOpened(sessionId);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void onSessionClosed() throws RemoteException {
		listener.onSessionClosed();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void onContextEvent(ContextEvent event) throws RemoteException {
		listener.onContextEvent(event);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void onContextSupportAdded(ContextSupportInfo supportInfo) throws RemoteException {
		listener.onContextSupportAdded(supportInfo);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void onContextSupportRemoved(ContextSupportInfo supportInfo) throws RemoteException {
		listener.onContextSupportRemoved(supportInfo);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void onContextTypeNotSupported(String contextType) throws RemoteException {
		listener.onContextTypeNotSupported(contextType);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void onInstallingContextSupport(ContextPluginInformation plugin, String contextType) throws RemoteException {
		listener.onInstallingContextSupport(plugin, contextType);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void onInstallingContextPlugin(ContextPluginInformation plugin) throws RemoteException {
		listener.onInstallingContextPlugin(plugin);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void onContextPluginInstallProgress(ContextPluginInformation plugin, int percentComplete)
			throws RemoteException {
		listener.onContextPluginInstallProgress(plugin, percentComplete);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void onContextPluginInstalled(ContextPluginInformation plugin) throws RemoteException {
		listener.onContextPluginInstalled(plugin);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void onContextPluginUninstalled(ContextPluginInformation plugin) throws RemoteException {
		listener.onContextPluginUninstalled(plugin);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void onContextPluginInstallFailed(ContextPluginInformation plug, String message) throws RemoteException {
		listener.onContextPluginInstallFailed(plug, message);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void onContextRequestFailed(String requestId, String message, int errorCode) throws RemoteException {
		listener.onContextRequestFailed(requestId, message, errorCode);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void onContextPluginDiscoveryStarted() throws RemoteException {
		listener.onContextPluginDiscoveryStarted();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void onContextPluginDiscoveryFinished(List<ContextPluginInformation> discoveredPlugins)
			throws RemoteException {
		listener.onContextPluginDiscoveryFinished(discoveredPlugins);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void onDynamixFrameworkActive() throws RemoteException {
		listener.onDynamixFrameworkActive();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void onDynamixFrameworkInactive() throws RemoteException {
		listener.onDynamixFrameworkInactive();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void onContextPluginError(ContextPluginInformation plug, String message) throws RemoteException {
		listener.onContextPluginError(plug, message);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void onContextPluginEnabled(ContextPluginInformation plug) throws RemoteException {
		listener.onContextPluginEnabled(plug);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void onContextPluginDisabled(ContextPluginInformation plug) throws RemoteException {
		listener.onContextPluginDisabled(plug);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void onContextSupportResult(ContextSupportResult result) throws RemoteException {
		listener.onContextSupportResult(result);
	}
}
