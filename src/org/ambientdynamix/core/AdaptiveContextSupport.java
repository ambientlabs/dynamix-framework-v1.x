/*
 * Copyright (C) The Ambient Dynamix Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ambientdynamix.core;

import java.util.ArrayList;
import java.util.List;

import org.ambientdynamix.api.application.ContextPluginInformation;
import org.ambientdynamix.api.application.IDynamixListener;
import org.ambientdynamix.api.contextplugin.ContextPlugin;
import org.ambientdynamix.api.contextplugin.ContextPluginRecommenderRuntime;
import org.ambientdynamix.api.contextplugin.IContextPluginRecommenderListener;
import org.ambientdynamix.api.contextplugin.PluginRecommendation;

import android.util.Log;

public class AdaptiveContextSupport extends ContextSupport implements IContextPluginRecommenderListener {
	private String TAG = this.getClass().getSimpleName();
	private ContextPluginRecommenderRuntime runtime;
	private List<ContextSupport> supportRegistrations = new ArrayList<ContextSupport>();

	public AdaptiveContextSupport(ContextPluginRecommenderRuntime runtime, DynamixSession session,
			IDynamixListener listener, ContextPlugin parentPlugin, String contextType) {
		super(session, listener, parentPlugin, contextType);
		this.runtime = runtime;
		Log.d(TAG, "Created AdaptiveContextSupport for " + runtime);
	}

	public void initContextPluginRecommenderListener() {
		this.runtime.addContextPluginRecommenderListener(this);
	}

	public void removeContextPluginRecommenderListener() {
		this.runtime.removeContextPluginRecommenderListener(this);
	}

	public void destroy() {
		if (runtime != null) {
			this.runtime.removeContextPluginRecommenderListener(this);
			this.runtime = null;
		}
		if (supportRegistrations != null) {
			this.supportRegistrations.clear();
			this.supportRegistrations = null;
		}
	}

	@Override
	public void onPluginRecommended(PluginRecommendation recommendation, boolean deprecateExistingPlugins) {
		Log.d(TAG, "onPluginRecommended " + recommendation);
		if (deprecateExistingPlugins) {
			List<ContextSupport> registrations = SessionManager.getContextSupport(getDynamixApplication(),
					getDynamixListener(), getContextType());
			for (ContextSupport reg : registrations) {
				/*
				 * Only remove the registration if it's NOT Dynamix managed (meaning we're managing it). Otherwise,
				 * we'll remove our own registration and Dynamix will deactivate our parent plug-in.
				 */
				if (!reg.getContextPlugin().isDynamixManaged())
					DynamixService.removeContextSupport(getDynamixApplication(), getDynamixListener(),
							reg.getContextSupportInfo());
			}
		}
		List<ContextSupport> supportList = DynamixService.addContextSupport(getDynamixApplication(),
				getDynamixListener(), recommendation.getPluginId(), getContextType(), true);
		if (supportList != null && supportList.size() > 0) {
			for (ContextSupport s : supportList) {
				if (!supportRegistrations.contains(s))
					supportRegistrations.add(s);
			}
		}
	}

	@Override
	public void onPluginsRecommended(List<PluginRecommendation> recommendations,
			boolean deprecateExistingPlugins) {
		if (deprecateExistingPlugins) {
			List<ContextSupport> registrations = SessionManager.getContextSupport(getDynamixApplication(),
					getDynamixListener(), getContextType());
			for (ContextSupport reg : registrations) {
				/*
				 * Only remove the registration if it's NOT Dynamix managed (meaning we're managing it). Otherwise,
				 * we'll remove our own registration and Dynamix will deactivate our parent plug-in.
				 */
				if (!reg.getContextPlugin().isDynamixManaged())
					DynamixService.removeContextSupport(getDynamixApplication(), getDynamixListener(),
							reg.getContextSupportInfo());
			}
		}
		for (PluginRecommendation rec : recommendations) {
			this.onPluginRecommended(rec, false);
		}
	}

	@Override
	public void onPluginDeprecated(ContextPluginInformation plug) {
		/*
		 * Note that we also need to solve the issue of requestId transferring
		 * between context providers, or sharing a request id amoung several
		 * providers.
		 */
		Log.w(TAG, "onPluginDeprecated " + plug);
		// for (ContextSupport reg : supportRegistrations) {
		// if (reg.getContextPlugin().getContextPluginInformation().equals(plug))
		// DynamixService.removeContextSupport(reg.getDynamixApplication(), reg.getDynamixListener(),
		// reg.getContextSupportInfo());
		// }
	}

	@Override
	public synchronized void onPluginsDeprecated(List<ContextPluginInformation> plugs) {
		for (ContextPluginInformation plug : plugs)
			this.onPluginDeprecated(plug);
	}
}
