/*
 * Copyright (C) The Ambient Dynamix Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ambientdynamix.update.contextplugin;

import java.io.InputStream;
import java.io.Reader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

import org.ambientdynamix.api.application.VersionInfo;
import org.ambientdynamix.api.contextplugin.PluginConstants.PLATFORM;
import org.ambientdynamix.util.RepositoryInfo;
import org.ambientdynamix.util.Utils;
import org.dom4j.Document;
import org.dom4j.io.SAXReader;
import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;

import android.util.Log;

public class SimpleSourceBase {
	private final String TAG = this.getClass().getSimpleName();
	
	
	protected List<PendingContextPlugin> createDiscoveredPlugins(RepositoryInfo repo, InputStream input,
			PLATFORM platform, VersionInfo platformVersion, VersionInfo frameworkVersion, boolean processSingle)
			throws Exception {
	
		List<PendingContextPlugin> plugs = new ArrayList<PendingContextPlugin>();
		//final Format format = new Format(0);
		Serializer serializer = new Persister();
		
		SAXReader reader = new SAXReader(); // dom4j SAXReader
		/*
		 * TODO: Using the dom4j Document here, since it can load input from a variety of sources automatically (file
		 * and network). We could explore providing our own low-overhead transport mechanisms, if the Document gets too
		 * heavy.
		 */
		Document document = reader.read(input);
		String xml = Utils.removeFormattingCharacters(document.asXML());
		input.close();
		Reader metaReader = new StringReader(xml);
		ContextPluginsBinder plugsBinder = serializer.read(ContextPluginsBinder.class, metaReader, false);
		for (ContextPluginBinder plugBinder : plugsBinder.contextPlugin) {
			try {
				PendingContextPlugin plug = plugBinder.createPendingPlugin(repo);
				if (UpdateUtils
						.checkCompatibility(plug.getPendingContextPlugin(), platform, platformVersion, frameworkVersion))
					plugs.add(plug);
			} catch (Exception e) {
				Log.w(TAG, "Exception creating plugin: " + plugBinder.id + " error was: " + e.toString());
			}
		}
		return plugs;
	}
}
