/*
 * Copyright (C) The Ambient Dynamix Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ambientdynamix.update.contextplugin;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;

import org.ambientdynamix.api.application.AppConstants.ContextPluginType;
import org.ambientdynamix.api.application.VersionInfo;
import org.ambientdynamix.api.contextplugin.ContextPlugin;
import org.ambientdynamix.api.contextplugin.ContextPluginDependency;
import org.ambientdynamix.api.contextplugin.DynamixFeatureInfo;
import org.ambientdynamix.api.contextplugin.PluginConstants.PLATFORM;
import org.ambientdynamix.api.contextplugin.PluginConstants.UpdatePriority;
import org.ambientdynamix.api.contextplugin.security.Permission;
import org.ambientdynamix.api.contextplugin.security.PrivacyRiskLevel;
import org.ambientdynamix.util.RepositoryInfo;
import org.ambientdynamix.util.Utils;
import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;

import android.util.Log;

/**
 * Simple Framework binder class for XML parsing.
 * 
 * @author Darren Carlson
 * 
 */
@Element(name = "contextPlugin")
public class ContextPluginBinder {
	private String TAG = getClass().getSimpleName();
	@Attribute(required = false)
	String metadataVersion;
	@Attribute(required = false)
	String repoType;
	@Attribute
	String id;
	@Attribute
	String pluginVersion;
	@Attribute
	String pluginType;
	@Attribute
	String provider;
	@Attribute
	String platform;
	@Attribute
	String minPlatformVersion;
	@Attribute(required = false)
	String maxPlatformVersion;
	@Attribute
	String minFrameworkVersion;
	@Attribute(required = false)
	String maxFrameworkVersion;
	@Attribute(required = false)
	boolean requiresConfiguration;
	@Attribute(required = false)
	boolean hasConfigurationView;
	@Attribute(required = false)
	String runtimeFactoryClass;
	@Element
	String name;
	@Element
	String description;
	@ElementList(entry = "privacyRiskLevel")
	List<PrivacyRiskLevelBinder> supportedPrivacyRiskLevels;
	@ElementList(entry = "supportedContextTypes")
	List<String> supportedContextTypes;
	@ElementList(entry = "usesFeature", required = false)
	List<FeatureDependencyBinder> featureDependencies;
	@ElementList(entry = "permissions", required = false)
	List<String> permissions;
	@Element(required = false)
	UpdateMessageBinder updateMessage;
	@Element(required = false)
	String installUrl;
	@Element(required = false)
	String updateUrl;
	@ElementList(entry = "contextPluginDependency", required = false)
	List<PluginDependencyBinder> contextPluginDependencies;
	@Attribute(required = false)
	boolean dynamixManaged = true;
	@Attribute(required = false)
	boolean backgroundService = false;

	public PendingContextPlugin createPendingPlugin(RepositoryInfo source) throws Exception {
		ContextPlugin newPlug = new ContextPlugin();
		newPlug.setId(id);
		newPlug.setRepoSource(source);
		newPlug.setTargetPlatform(PLATFORM.getPlatformFromString(platform));
		newPlug.setVersionInfo(VersionInfo.createVersionInfo(pluginVersion));
		newPlug.setMinPlatformApiLevel(VersionInfo.createVersionInfo(minPlatformVersion));
		// Check for optional max platform
		if (maxPlatformVersion != null && maxPlatformVersion.length() > 0)
			newPlug.setMaxPlatformApiLevel(VersionInfo.createVersionInfo(maxPlatformVersion));
		newPlug.setMinFrameworkVersion(VersionInfo.createVersionInfo(minFrameworkVersion));
		// Check for optional max Dynamix framework
		if (maxFrameworkVersion != null && maxFrameworkVersion.length() > 0)
			newPlug.setMaxFrameworkVersion(VersionInfo.createVersionInfo(maxFrameworkVersion));
		newPlug.setProvider(provider);
		newPlug.setName(name);
		newPlug.setDescription(description);
		// Setup the plug-in's type
		newPlug.setContextPluginType(Utils.getEnumFromString(ContextPluginType.class, pluginType));
		if (newPlug.getContextPluginType() != ContextPluginType.LIBRARY) {
			if (runtimeFactoryClass == null)
				throw new Exception("Missing runtime factory class");
		}
		newPlug.setRequiresConfiguration(requiresConfiguration);
		newPlug.setHasConfigurationView(hasConfigurationView);
		newPlug.setRuntimeFactoryClass(runtimeFactoryClass);
		// Setup supported privacy risk levels
		Map<PrivacyRiskLevel, String> riskLevelsMap = new HashMap<PrivacyRiskLevel, String>();
		for (PrivacyRiskLevelBinder rl : supportedPrivacyRiskLevels) {
			PrivacyRiskLevel l = PrivacyRiskLevel.getLevelForString(rl.name);
			if (l != null && !riskLevelsMap.containsKey(l))
				riskLevelsMap.put(l, description);
			else
				Log.w(TAG, "PrivacyRiskLevel null or duplicated: " + l);
		}
		newPlug.setSupportedPrivacyRiskLevels(riskLevelsMap);
		// Setup supported context types
		newPlug.setSupportedContextTypes(supportedContextTypes);
		newPlug.setPermissions(new LinkedHashSet<Permission>());
		// Setup permissions
		for (String permissionString : permissions) {
			Permission p = Permission.createPermission(permissionString);
			if (p != null) {
				// TODO: For now we grant all permissions - update this
				p.setPermissionGranted(true);
				newPlug.getPermissions().add(p);
				Log.w(TAG, "For testing, we are automatically granting Permission: " + permissionString + " to "
						+ newPlug);
			}
		}
		Log.w(TAG, "Automatically granting Permissions.LAUNCH_CONTEXT_ACQUISITION_ACTIVITY");
		newPlug.getPermissions().add(Permission.createPermission("Permissions.LAUNCH_CONTEXT_ACQUISITION_ACTIVITY"));
		// Setup feature dependencies
		newPlug.setFeatureDependencies(new LinkedHashSet<DynamixFeatureInfo>());
		if (featureDependencies != null)
			for (FeatureDependencyBinder f : featureDependencies) {
				newPlug.getFeatureDependencies().add(new DynamixFeatureInfo(f.name, f.required));
			}
		newPlug.setInstallUrl(installUrl);
		newPlug.setUpdateUrl(updateUrl);
		// Set the plug-in's dependencies
		if (contextPluginDependencies != null) {
			List<ContextPluginDependency> dep = new ArrayList<ContextPluginDependency>();
			for (PluginDependencyBinder p : contextPluginDependencies) {
				if (p.pluginVersion != null)
					dep.add(new ContextPluginDependency(p.pluginId, VersionInfo.createVersionInfo(p.pluginVersion),
							p.pluginName, p.installUrl));
				else
					dep.add(new ContextPluginDependency(p.pluginId, null, p.pluginName, p.installUrl));
			}
			newPlug.setPluginDependencies(dep);
		} else
			Log.i(TAG, "No Dependencies for " + newPlug.getId());
		newPlug.setDynamixManaged(dynamixManaged);
		newPlug.setBackgroundService(backgroundService);
		// Validate the plugin and add it to the list of ContextPluginUpdates
		Log.d(TAG, "Validating new plug-in: " + newPlug);
		if (Utils.validateContextPlugin(newPlug)) {
			Log.d(TAG, "Plug-in is valid: " + newPlug);
			if (updateMessage != null) {
				String messsage = updateMessage.message;
				UpdatePriority priority = UpdatePriority.valueOf(updateMessage.priority);
				return new PendingContextPlugin(newPlug, messsage, priority);
			} else
				return new PendingContextPlugin(newPlug);
		} else
			throw new Exception("Context Plugin Invalid");
	}
}
