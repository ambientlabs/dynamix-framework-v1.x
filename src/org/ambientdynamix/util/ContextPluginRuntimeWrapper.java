/*
 * Copyright (C) The Ambient Dynamix Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ambientdynamix.util;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.LinkedBlockingQueue;

import org.ambientdynamix.api.contextplugin.ContextPluginRuntime;
import org.ambientdynamix.api.contextplugin.IPluginStateListener;
import org.ambientdynamix.api.contextplugin.Message;
import org.ambientdynamix.api.contextplugin.PluginState;

import android.util.Log;

/**
 * Stateful wrapper for ContextPluginRuntimes.
 * 
 * @author Darren Carlson
 */
public class ContextPluginRuntimeWrapper {
	private final String TAG = this.getClass().getSimpleName();
	// Private data
	private volatile PluginState currentState;
	private volatile ContextPluginRuntime runtime;
	private volatile boolean executing;
	private List<IPluginStateListener> stateListeners = new ArrayList<IPluginStateListener>();
	private final LinkedBlockingQueue<Message> messageQ = new LinkedBlockingQueue<Message>();
	private boolean processingMessages = false;


	/**
	 * Creates a ContextPluginRuntimeWrapper using the specified ContextPluginRuntime and PluginState.
	 * 
	 * @param runtime
	 * @param initialState
	 */
	public ContextPluginRuntimeWrapper(ContextPluginRuntime runtime, PluginState initialState) {
		this.runtime = runtime;
		this.setState(initialState);
	}

	/**
	 * Adds a IPluginStateListener.
	 */
	public void addStateListener(IPluginStateListener listener) {
		synchronized (stateListeners) {
			if (!stateListeners.contains(listener))
				stateListeners.add(listener);
		}
	}

	/**
	 * Removes a previously registered IPluginStateListener.
	 */
	public boolean removeStateListener(IPluginStateListener listener) {
		synchronized (stateListeners) {
			return stateListeners.remove(listener);
		}
	}

	/**
	 * Clears all current state listeners.
	 */
	public void clearAllStateListeners() {
		synchronized (stateListeners) {
			stateListeners.clear();
		}
	}

	/**
	 * Creates a ContextPluginRuntimeWrapper with state new.
	 */
	public ContextPluginRuntimeWrapper() {
		setState(PluginState.NEW);
	}

	/**
	 * Returns true if the runtime is executing (started).
	 * 
	 * @return
	 */
	public boolean isExecuting() {
		return executing;
	}

	/**
	 * Set true if the runtime is executing (started); false otherwise.
	 * 
	 * @param executing
	 */
	public void setExecuting(boolean executing) {
		this.executing = executing;
	}

	/**
	 * Returns the wrapped ContextPluginRuntime.
	 */
	public ContextPluginRuntime getContextPluginRuntime() {
		return runtime;
	}

	/**
	 * Sets the ContextPluginRuntime.
	 */
	public void setContextPluginRuntime(ContextPluginRuntime runtime) {
		this.runtime = runtime;
	}

	/**
	 * Returns the current PluginState.
	 * 
	 * @see PluginState
	 */
	public final synchronized PluginState getState() {
		return this.currentState;
	}

	/**
	 * Sets the current PluginState.
	 * 
	 * @see PluginState
	 */
	public final synchronized void setState(PluginState newState) {
		if (newState == null)
			throw new RuntimeException("PluginState cannot be NULL!");
		else {
			this.currentState = newState;
			if (newState == PluginState.STARTED) {
				if (!processingMessages) {
					processingMessages = true;
					Utils.dispatch(true, new Runnable() {
						@Override
						public void run() {
							messageProcessorLoop();
						}
					});
				}
			} else if (newState == PluginState.DESTROYED) {
				clearMessageQueue();
			} else {
				// Stop message processing
				processingMessages = false;
			}
			// Update listeners
			Utils.dispatch(true, new Runnable() {
				@Override
				public void run() {
					for (IPluginStateListener listener : stateListeners) {
						if (currentState == PluginState.NEW) {
							listener.onNew(runtime.getParentPlugin().getContextPluginInformation());
						}
						if (currentState == PluginState.INITIALIZING) {
							listener.onInitialized(runtime.getParentPlugin().getContextPluginInformation());
						}
						if (currentState == PluginState.INITIALIZED) {
							listener.onInitialized(runtime.getParentPlugin().getContextPluginInformation());
						}
						if (currentState == PluginState.STARTING) {
							listener.onStarting(runtime.getParentPlugin().getContextPluginInformation());
						}
						if (currentState == PluginState.STARTED) {
							listener.onStarted(runtime.getParentPlugin().getContextPluginInformation());
						}
						if (currentState == PluginState.STOPPING) {
							listener.onStopping(runtime.getParentPlugin().getContextPluginInformation());
						}
						if (currentState == PluginState.ERROR) {
							listener.onError(runtime.getParentPlugin().getContextPluginInformation());
						}
						if (currentState == PluginState.DESTROYED) {
							listener.onDestroyed(runtime.getParentPlugin().getContextPluginInformation());
						}
					}
				}
			});
		}
	}
	

	/**
	 * Adds the message to the message queue, which processes messages when the plug-in is in state STARTED.
	 * 
	 */
	public final void enqueueMessage(Message m) {
		messageQ.add(m);
	}

	/**
	 * Clears the message queue.
	 */
	public final void clearMessageQueue() {
		messageQ.clear();
	}

	private synchronized void messageProcessorLoop() {
		Log.v(TAG, "Starting message processing");
		while (!processingMessages) {
			try {
				// Try to grab an installed (the queue will block until something is inserted)
				Message m = messageQ.take();
				runtime.onMessageReceived(m);
			} catch (Exception e) {
				Log.w(TAG, "Message processor exception: " + e);
			}
		}
		Log.v(TAG, "Stopped message processing");
	}
}
