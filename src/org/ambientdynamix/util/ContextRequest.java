/*
 * Copyright (C) The Ambient Dynamix Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ambientdynamix.util;

import java.util.Date;

import org.ambientdynamix.api.application.IDynamixListener;
import org.ambientdynamix.api.contextplugin.ContextPlugin;
import org.ambientdynamix.core.DynamixApplication;

import android.os.Bundle;

/**
 * Represents a context request for a specific DynamixApplication and IDynamixListener. By default, the request is not
 * persistent, which means that only a single response to this request is allowed and Dynamix clears the request state
 * once the response is sent from the plug-in to the requesting app. It is also possible to make a ContextRequest
 * persistent, which allows multiple responses to be sent from plug-ins to apps using the same requestId. Note that both
 * non-persistent and persistent ContextRequests must complete before the request timeout (e.g., 30 seconds) or they
 * will be automatically removed by Dynamix. To extend the processing time provided to requests, plug-ins may 'ping' the
 * request, which resets the timeout, providing additional time to complete.
 * 
 * @author Darren Carlson
 */
public class ContextRequest {
	// Private data
	private DynamixApplication app;
	private IDynamixListener listener;
	private String contextType;
	private Date createdTime;
	private Date lastPing;
	private ContextPlugin plugin;
	private boolean persistent = false;
	private boolean autoClean = true;
	private Bundle requestConfig;

	/**
	 * Creates a ContextRequest for the specified DynamixApplication and IDynamixListener.
	 */
	public ContextRequest(DynamixApplication app, IDynamixListener listener, ContextPlugin plugin, String contextType,
			Bundle requestConfig) {
		this.app = app;
		this.listener = listener;
		this.createdTime = new Date();
		this.lastPing = new Date();
		this.plugin = plugin;
		this.contextType = contextType;
		this.requestConfig = requestConfig;
		this.autoClean = false;
	}

	/**
	 * Creates a ContextRequest for the specified DynamixApplication and IDynamixListener.
	 */
	public ContextRequest(DynamixApplication app, IDynamixListener listener, ContextPlugin plugin, String contextType,
			Bundle requestConfig, boolean autoClean) {
		this(app, listener, plugin, contextType, requestConfig);
		this.autoClean = autoClean;
	}

	/**
	 * Returns the context type associated with the request.
	 */
	public String getContextType() {
		return contextType;
	}

	/**
	 * Returns the configuration Bundle associated with the request, or null if there isn't one.
	 */
	public Bundle getRequestConfig() {
		return requestConfig;
	}

	/**
	 * Returns true if the request has an associated configuration; false otherwise.
	 * 
	 * @return
	 */
	public boolean hasRequestConfig() {
		return requestConfig != null;
	}

	@Override
	public boolean equals(Object candidate) {
		// first determine if they are the same object reference
		if (this == candidate)
			return true;
		// make sure they are the same class
		if (candidate == null || candidate.getClass() != getClass())
			return false;
		ContextRequest other = (ContextRequest) candidate;
		return this.app.equals(other.app) && listener.asBinder().equals(other.listener.asBinder())
				&& this.plugin.equals(other.plugin) ? true : false;
	}

	/**
	 * Pings the request, which keeps the request alive for another timeout interval (e.g., gives the plug-in another 30
	 * seconds to finish processing).
	 */
	public void ping() {
		this.lastPing = new Date();
	}

	/**
	 * Returns true if this request is persistent, which means that the request is retained by Dynamix so that multiple
	 * events can be sent can be sent from the plug-in to an app using the same requestId; false otherwise.
	 */
	public boolean isPersistent() {
		return this.persistent;
	}

	/**
	 * Set true if this request is persistent, which means that the request is retained by Dynamix so that multiple
	 * events can be sent can be sent from the plug-in to an app using the same requestId; false otherwise.
	 */
	public void setPersistent(boolean persistent) {
		this.persistent = persistent;
	}

	/**
	 * Returns true if Dynamix should automatically remove this request from the requestMap when it expires.
	 */
	public boolean autoClean() {
		return this.autoClean;
	}

	/**
	 * Returns the DynamixApplication that made the request.
	 */
	public DynamixApplication getApp() {
		return app;
	}

	/**
	 * Returns the IDynamixListener that made the request.
	 */
	public IDynamixListener getListener() {
		return listener;
	}

	/**
	 * Returns the time that this context request was created
	 */
	public Date getCreatedTime() {
		return this.createdTime;
	}

	/**
	 * Returns the time that this context request was last pinged
	 */
	public Date getLastPing() {
		return this.lastPing;
	}

	/**
	 * Returns true if the request has expired (i.e., the last ping was more than maxAgeMills ago); false if the request
	 * is still valid.
	 */
	public boolean isExpired(long maxAgeMills) {
		return (new Date().getTime() - this.lastPing.getTime()) > maxAgeMills;
	}

	/**
	 * Returns the Context Plug-in handling this context request.
	 */
	public ContextPlugin getPlugin() {
		return this.plugin;
	}

	@Override
	public int hashCode() {
		int result = 17;
		result = 31 * result + app.hashCode() + listener.hashCode() + plugin.hashCode();
		return result;
	}
}
